#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

struct fb {
	size_t size;
	struct fb* next;
	bool is_free;
};

// première zone de la mémoire
struct fb * mem_zone_head;

// initialise la mémoire
void mem_init(void* mem, size_t taille)
{
	assert(mem == get_memory_adr());
	assert(taille == get_memory_size());

	mem_zone_head = (struct fb*) mem;
	mem_zone_head->size = (size_t) (taille - sizeof(struct fb));
	mem_zone_head->next = NULL;
	mem_zone_head->is_free = true;

	mem_fit(&mem_fit_first);
}

// montre l'état de la mémoire
void mem_show(void (*print)(void *, size_t, int)) {
	struct fb* tmp = mem_zone_head;

	// parcours de la mémoire
	while(tmp) {
		// affiche l'adresse de la zone, la taille allouée et si elle est libre
		print(tmp, tmp->size, tmp->is_free);
		tmp = tmp->next;
	}
}

static mem_fit_function_t *mem_fit_fn;

void mem_fit(mem_fit_function_t *f) {
	mem_fit_fn = f;
}

void * mem_alloc(size_t size) {
	// ffz pointera sur la première zone libre
	// ffz: first free zone
	// on ajoute ici la taille d'une struct fb afin de pouvoir être capable de réallouer de la mémoire dans le résidu
	struct fb *ffz = mem_fit_fn(mem_zone_head, size + sizeof(struct fb));

	if(ffz) {
		// nouv_ffz va pointer sur l'adresse du résidu (de la zone libre restante)
		void * p_ffz = (void *)ffz + size + sizeof(struct fb);
		struct fb * nouv_ffz = (struct fb *)p_ffz;

		// on calcule la taille du résidu
		if((ffz->size - size - sizeof(struct fb)) >= 0)
			nouv_ffz->size = (size_t) (ffz->size - size - sizeof(struct fb));
		else
			nouv_ffz->size = 0;
		nouv_ffz->is_free = true;

		// la zone allouée est maintenant occupée
		ffz->is_free = false;
		ffz->size = size;

		// on insère notre nouvelle zone libre dans la liste
		nouv_ffz->next = ffz->next;
		ffz->next = nouv_ffz;

		return ffz;
	}
	else
		return NULL;
}

// libère une zone occupée à l'adresse mem
void mem_free(void* mem) {
	// ztf est l'adresse de la zone à libérer
	// ztf: zone to free
	struct fb * ztf = (struct fb *) mem;
	struct fb * next_zone = ztf->next;

	// on récupère l'adresse de la zone précédant la zone à libérer
	struct fb *prev = mem_zone_head;
	while(prev && prev->next != ztf)
		prev = prev->next;
	// concaténation avec la zone précédente
	if(prev && prev->is_free) {
		prev->size += sizeof(struct fb) + ztf->size;
		// on saute et oublie la zone
		prev->next = ztf->next;
	}

	// concatène avec la zone suivante
	if(next_zone && next_zone->is_free) {
		ztf->size += next_zone->size + sizeof(struct fb);

		ztf->next = next_zone->next;
	}

	// la zone est enfin libérée
	ztf->is_free = true;
}

// retourne l'adresse de la première zone mémoire plus grande que size
// retourne NULL dans le cas où aucune zone n'est plus grande que size
struct fb* mem_fit_first(struct fb *list, size_t size) {
	struct fb* tmp = mem_zone_head;

	// on parcourt toutes les zones libres
	while(tmp) {
		if(tmp->is_free && tmp->size >= size)
			return tmp;
		tmp = tmp->next;
	}

	return NULL;
}

/* Fonction à faire dans un second temps
* - utilisée par realloc() dans malloc_stub.c
* - nécessaire pour remplacer l'allocateur de la libc
* - donc nécessaire pour 'make test_ls'
* Lire malloc_stub.c pour comprendre son utilisation
* (ou en discuter avec l'enseignant)
*/
size_t mem_get_size(void*zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	* l'utilisateur peut utiliser dans cette zone */
	return ((struct fb *) zone)->size;
}

/* Fonctions facultatives
* autres stratégies d'allocation
*/
struct fb* mem_fit_best(struct fb *list, size_t size) {
	struct fb* fit_best = mem_zone_head;
	struct fb* tmp = mem_zone_head;

	// on parcourt toutes les zones libres et on renvoie celle qui aurait le plus petit résidu
	while(tmp) {
		if(tmp->is_free && tmp->size >= size && (!fit_best || tmp->size < fit_best->size))
			fit_best = tmp;
		tmp = tmp->next;
	}
	return fit_best;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	struct fb* fit_worst = mem_zone_head;
	struct fb* tmp = mem_zone_head;

	// on parcourt toutes les zones libres et on renvoie celle qui aurait le plus grand résidu
	while(tmp) {
		if(tmp->is_free && tmp->size >= size && (!fit_worst || tmp->size > fit_worst->size))
			fit_worst = tmp;
		tmp = tmp->next;
	}
	return fit_worst;
}
